package com.datamond.crm.service;

import com.datamond.crm.entity.Sold;
import com.datamond.crm.repository.ProductRepo;
import com.datamond.crm.repository.SoldRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SoldService {

    @Autowired
    private SoldRepo soldRepo;

    public boolean checkTodayDate(String date, Integer productId){
       return soldRepo.checkIfDateExists(date,productId);
    }

    public boolean checkProductinSold(Integer productId){
        return soldRepo.checkIfProductExists(productId);
    }

    public void saveSold(String productName, int soldQuantity,String date,int productId){
        Sold sold = new Sold(productName,soldQuantity,date,productId);
        soldRepo.save(sold);
    }

    public void updateSoldQuantity(Integer quantity,String date,Integer productId){
        soldRepo.updateQuantity(quantity,date,productId);
    }

    public List findAllSoldProducts(){
      return  soldRepo.findAll();
    }



    public void updateSoldWhileProductUpdated(String productName, Integer id) {
        soldRepo.updateSoldAsProduct(productName,id);
    }
}
