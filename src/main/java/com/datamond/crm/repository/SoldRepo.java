package com.datamond.crm.repository;

import com.datamond.crm.entity.Sold;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface SoldRepo extends JpaRepository<Sold,Integer> {

    @Query(value = "select exists(select *  from sold  where date= ?1 and product_id=?2 )",nativeQuery = true)
    boolean checkIfDateExists(String currentDate,Integer productId);

    @Query(value = "select exists(select *  from sold  where product_id=?1 )",nativeQuery = true)
    boolean checkIfProductExists(Integer productId);


    @Modifying
    @Transactional
    @Query(value = "update sold  set soldquantity=soldquantity+?1 " +
            "                            where date = ?2 and product_id= ?3",nativeQuery = true)
    void updateQuantity(Integer quantity,String date,Integer productId);

    @Modifying
    @Transactional
    @Query(value = "update sold  set product_name=?1 where product_id=?2",nativeQuery = true)
    void updateSoldAsProduct(String productName,Integer id);
}
