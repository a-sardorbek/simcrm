package com.datamond.crm.controller;


import com.datamond.crm.entity.Item;
import com.datamond.crm.entity.Product;
import com.datamond.crm.entity.Sold;
import com.datamond.crm.entity.UnitType;
import com.datamond.crm.repository.ItemRepo;
import com.datamond.crm.repository.ProductRepo;
import com.datamond.crm.service.ProductService;
import com.datamond.crm.service.SoldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class ProductController {

    @Autowired
    private ProductRepo productRepo;

    @Autowired
    private ItemRepo itemRepo;

    @Autowired
    private ProductService productService;

    @Autowired
    private SoldService soldService;

    @GetMapping("/products/new")
    public String newProduct(Model model){
        List<Item> itemList = itemRepo.findAll();
        model.addAttribute("itemList", itemList);
        model.addAttribute("product",new Product());
        model.addAttribute("unitTypes", UnitType.values());
        return "product_form";
    }

    @PostMapping("/products/save")
    public String saveProduct(Product product){
        productRepo.save(product);

        if(soldService.checkProductinSold(product.getId())) {
         soldService.updateSoldWhileProductUpdated(product.getName(),product.getId());
        }

        return "redirect:/products";
    }

    @GetMapping("/products")
    public String listProducts(Model model){
        List<Product> listProducts = productRepo.findAll();
        model.addAttribute("listProducts",listProducts);
        return "products";
    }

    @GetMapping("products/edit/{id}")
    public String editProduct(@PathVariable("id") Integer id, Model model){
        Product product;
        product = productRepo.findById(id).get();

        List<Item> itemList = itemRepo.findAll();
        model.addAttribute("itemList", itemList);
        model.addAttribute("product",product);
        model.addAttribute("unitTypes", UnitType.values());
//        model.addAttribute("sold", sold);


        return "product_form";
    }

    @GetMapping("products/delete/{id}")
    public String deleteProduct(@PathVariable("id") Integer id, Model model){
        productRepo.deleteById(id);
        return "redirect:/products";
    }

    @GetMapping("products/sell/{id}")
    public String sellProduct(@PathVariable("id") Integer id, Model model){
        Product product = productRepo.findById(id).get();
        model.addAttribute("product",product);
        model.addAttribute("unitTypes", UnitType.values());
        return "sold_products";
    }

    @PostMapping("/sold_products/save")
    public String saveSoldProduct(Product product, Sold sold){

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String currentDate = formatter.format(new Date());

        sold = new Sold();
        sold.setDate(currentDate);
        sold.setProductName(product.getName());
        sold.setSoldquantity(product.getSoldCount());
        sold.setProductId(product.getId());

        if(soldService.checkTodayDate(currentDate,product.getId())==true){
            //update
            soldService.updateSoldQuantity(sold.getSoldquantity(),sold.getDate(),sold.getProductId());
        }else {
            //insert
            soldService.saveSold(sold.getProductName(),sold.getSoldquantity(),sold.getDate(), sold.getProductId());
        }

        productService.updateQuantityIProduct(product.getSoldCount(), product.getId());

        return "redirect:/products";
    }



    @GetMapping("/soldProducts")
    public String showSoldProducts(Model model){
        List<Sold> solds = soldService.findAllSoldProducts();
        model.addAttribute("soldProducts",solds);
        return "sold_products_table";
  }



}
